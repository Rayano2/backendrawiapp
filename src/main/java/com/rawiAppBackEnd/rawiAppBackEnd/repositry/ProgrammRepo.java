package com.rawiAppBackEnd.rawiAppBackEnd.repositry;

import com.rawiAppBackEnd.rawiAppBackEnd.entity.Program;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProgrammRepo extends MongoRepository<Program, ObjectId> {

    Program findByProgrammName(String programmName);
}
