package com.rawiAppBackEnd.rawiAppBackEnd.repositry;


import com.rawiAppBackEnd.rawiAppBackEnd.entity.Program;
import com.rawiAppBackEnd.rawiAppBackEnd.entity.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepo  extends MongoRepository <User , Long> {


     User findUserByEmail(String email);
     User findUserByEmailAndMobileNumber(String email , String mobileNumber);
     List <User> findAll();
     @Query(value = "{from: \"program\",\n" +
             "    localField: \"programForSubscriper.programId\",\n" +
             "    foreignField: \"_id\",\n" +
             "    as: \"programms\"}")
     List<User> findProgrammsForTrainner();
     User findUserByIdAndUserType(Long id, Integer userType);

//     Optional<User> findUserBy

}
