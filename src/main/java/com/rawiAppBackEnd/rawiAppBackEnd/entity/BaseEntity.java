package com.rawiAppBackEnd.rawiAppBackEnd.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rawiAppBackEnd.rawiAppBackEnd.utils.Util;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;


import javax.annotation.PostConstruct;
import java.util.Date;
@Data
public class BaseEntity {

    @Id
    private Long id;
    @CreatedDate
    @JsonIgnore
    private Date createdOn;
    @JsonIgnore
    private Date updatedOn;


    public BaseEntity() {
        if (this.createdOn == null){
            this.createdOn = new Date();
        }
    }






}

