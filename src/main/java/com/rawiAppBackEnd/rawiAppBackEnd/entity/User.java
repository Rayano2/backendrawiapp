package com.rawiAppBackEnd.rawiAppBackEnd.entity;

import com.rawiAppBackEnd.rawiAppBackEnd.model.Subscription;
import com.rawiAppBackEnd.rawiAppBackEnd.utils.Util;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.annotation.PostConstruct;
import java.util.List;


@Data
@Document
public class User extends BaseEntity {


    private String username;
    private String password;
    @Indexed(unique = true)
    private String mobileNumber;
    @Indexed(unique = true)
    private String email;
    private Integer userType;
    @DBRef
    private List <Program>  programs;
    private List<Subscription> programForSubscriper ;


}
