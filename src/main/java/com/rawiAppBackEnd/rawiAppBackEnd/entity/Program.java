package com.rawiAppBackEnd.rawiAppBackEnd.entity;


import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Document
@Data
public class Program  extends BaseEntity{

    private String programmName;
    private String description;
    private Integer programLevel;
    private BigDecimal price;
    private String videoUrl;

}
