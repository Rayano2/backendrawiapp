package com.rawiAppBackEnd.rawiAppBackEnd;

import com.rawiAppBackEnd.rawiAppBackEnd.repositry.UserRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

import java.time.LocalDateTime;

@SpringBootApplication
@EnableMongoAuditing
public class RawiAppBackEndApplication implements CommandLineRunner {

	@Autowired
	UserRepo userRepo;
	private static final Logger log = LoggerFactory.getLogger(RawiAppBackEndApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(RawiAppBackEndApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
	  log.info(String.valueOf(LocalDateTime.now()));
	}
}
