package com.rawiAppBackEnd.rawiAppBackEnd.service;

import com.rawiAppBackEnd.rawiAppBackEnd.dto.LoginDto;

public interface LoginService {

    Object checkLogin(LoginDto loginDto);
}
