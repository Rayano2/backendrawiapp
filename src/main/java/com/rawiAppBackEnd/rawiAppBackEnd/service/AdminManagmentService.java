package com.rawiAppBackEnd.rawiAppBackEnd.service;

import com.rawiAppBackEnd.rawiAppBackEnd.dto.AddProgrammDto;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.CreateUserDto;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.GetSubscriberDto;
import com.rawiAppBackEnd.rawiAppBackEnd.model.Response.GeneralResponse;

public interface AdminManagmentService {

    GeneralResponse  getSubscriber(GetSubscriberDto getSubscriberDto);
    void createUser (CreateUserDto createUserDto);
    void addProgramm(AddProgrammDto addProgrammDto);

}
