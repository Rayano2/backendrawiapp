package com.rawiAppBackEnd.rawiAppBackEnd.service;

import com.rawiAppBackEnd.rawiAppBackEnd.dto.CreateUserDto;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.ResponseDto.AddSubscriptionDto;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.SubscriptionProgramDto;
import com.rawiAppBackEnd.rawiAppBackEnd.entity.Program;
import com.rawiAppBackEnd.rawiAppBackEnd.entity.User;
import com.rawiAppBackEnd.rawiAppBackEnd.model.Response.GeneralResponse;
import com.rawiAppBackEnd.rawiAppBackEnd.model.Subscription;
import com.rawiAppBackEnd.rawiAppBackEnd.repositry.ProgrammRepo;
import com.rawiAppBackEnd.rawiAppBackEnd.repositry.UserRepo;
import com.rawiAppBackEnd.rawiAppBackEnd.enums.UserTypeEnum;
import com.rawiAppBackEnd.rawiAppBackEnd.utils.Util;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.Objects;

@Service
public class UserManagmentServiceImple implements UserManagmentService {

    private UserRepo userRepo;
    private  PasswordEncoder passwordEncoder;
    private ProgrammRepo programmRepo;
    private Util util;

    @Autowired
    public UserManagmentServiceImple(UserRepo userRepo, PasswordEncoder passwordEncoder ,
                                     ProgrammRepo programmRepo , Util util) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
        this.programmRepo = programmRepo;
        this.util = util;

    }

    @Override
    public void addUser(CreateUserDto createUserDto) {
        User userSaved = new User();
        BeanUtils.copyProperties(createUserDto , userSaved);
        userSaved.setUserType(UserTypeEnum.USER.getCode());
        userSaved.setId(generateUniqueId());
        userSaved.setPassword(passwordEncoder.encode(createUserDto.getPassword()));
         userRepo.save(userSaved);
        return;
    }

    @Override
    public GeneralResponse subscriptionProgram(SubscriptionProgramDto subscriptionProgramDto) {
        GeneralResponse generalResponse = new GeneralResponse();
        Subscription subscription = new Subscription();
        User user = checkUserExistByEmailAndMobileNumber(subscriptionProgramDto.getMobileNumber(), subscriptionProgramDto.getEmail());
        if (Objects.nonNull(user)){
            Program program = programmRepo.findByProgrammName(subscriptionProgramDto.getProgramName());
            if (Objects.nonNull(program)){
                BeanUtils.copyProperties(subscriptionProgramDto , subscription);
                subscription.setProgramId(program);
                subscription.setSubscriptionStart(new Date());
                subscription.setActive(Boolean.FALSE);
                subscription.setSubscriptionId(12323234L);
                user.setProgramForSubscriper(Collections.singletonList(subscription));
                userRepo.save(user);
            }
        }

        return generalResponse;
    }

    private User checkUserExistByEmailAndMobileNumber (String mobileNumber , String email) {
        return userRepo.findUserByEmailAndMobileNumber(email , mobileNumber);
    }
    private Long generateUniqueId () {
        return util.getUniqueGenratorId();
    }
}
