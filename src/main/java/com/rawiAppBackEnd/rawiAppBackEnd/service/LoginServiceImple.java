package com.rawiAppBackEnd.rawiAppBackEnd.service;

import com.rawiAppBackEnd.rawiAppBackEnd.dto.LoginDto;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.ResponseDto.LoginResponseDto;
import com.rawiAppBackEnd.rawiAppBackEnd.entity.User;
import com.rawiAppBackEnd.rawiAppBackEnd.model.Response.GeneralResponse;
import com.rawiAppBackEnd.rawiAppBackEnd.repositry.UserRepo;
import com.rawiAppBackEnd.rawiAppBackEnd.enums.EstatusLogin;
import com.rawiAppBackEnd.rawiAppBackEnd.utils.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Slf4j
public class LoginServiceImple implements LoginService {

    private UserRepo userRepo;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public LoginServiceImple (UserRepo userRepo , PasswordEncoder passwordEncoder) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public GeneralResponse <LoginResponseDto> checkLogin(LoginDto loginDto) {
        LoginResponseDto loginResponseDto = new LoginResponseDto();
        GeneralResponse generalResponse = new GeneralResponse();
        User user = userRepo.findUserByEmail(loginDto.getUsername());
        if (Objects.nonNull(user)  &&
                passwordEncoder.matches(loginDto.getPassword() ,
                        Util.splitHasingIdPassword(user.getPassword()))){
                loginResponseDto.setStatus(EstatusLogin.SUCCESS.getMessage());
                loginResponseDto.setUsername(loginDto.getUsername());

        }else {
            loginResponseDto.setStatus(EstatusLogin.FAIELD.getMessage());
            loginResponseDto.setUsername(loginDto.getUsername());
        }
        generalResponse.setData(loginResponseDto);
        return generalResponse;
    }
}
