package com.rawiAppBackEnd.rawiAppBackEnd.service;



import com.mongodb.BasicDBObject;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.AddProgrammDto;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.CreateUserDto;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.GetSubscriberDto;
import com.rawiAppBackEnd.rawiAppBackEnd.entity.Program;
import com.rawiAppBackEnd.rawiAppBackEnd.entity.User;
import com.rawiAppBackEnd.rawiAppBackEnd.model.Response.GeneralResponse;
import com.rawiAppBackEnd.rawiAppBackEnd.repositry.ProgrammRepo;
import com.rawiAppBackEnd.rawiAppBackEnd.repositry.UserRepo;
import com.rawiAppBackEnd.rawiAppBackEnd.enums.UserTypeEnum;
import com.rawiAppBackEnd.rawiAppBackEnd.utils.Util;
import lombok.extern.slf4j.Slf4j;
import org.bson.conversions.Bson;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.Arrays;
import org.bson.Document;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.exists;
import static com.rawiAppBackEnd.rawiAppBackEnd.config.constants.Constans.*;

import java.util.*;

@Service
@Slf4j
public class AdminManagmentServiceImple  implements AdminManagmentService {

    private ProgrammRepo programmRepo;
    private UserRepo userRepo;
    private PasswordEncoder passwordEncoder;
    private Util util;
    private  MongoOperations mongoOperations ;
    private MongoTemplate mongoTemplate;


    @Autowired
    public AdminManagmentServiceImple (ProgrammRepo programmRepo ,
                                       UserRepo userRepo,
                                       Util util,
                                       PasswordEncoder passwordEncoder ,
                                       MongoOperations mongoOperations ,
                                       MongoTemplate mongoTemplate) {
      this.programmRepo = programmRepo;
      this.userRepo = userRepo;
      this.passwordEncoder = passwordEncoder;
      this.util = util;
      this.mongoOperations = mongoOperations;
      this.mongoTemplate = mongoTemplate;
    }

    @Override
    public GeneralResponse getSubscriber(GetSubscriberDto getSubscriberDto) {
        GeneralResponse<List<BasicDBObject>> generalResponse = new GeneralResponse<>();
        User trainner = getTrainnerById(getSubscriberDto.getTrainnerId());
        if (Objects.nonNull(trainner)) {
            LookupOperation lookupOperation = LookupOperation.newLookup().
                    from(FROM).
                    localField(LOCAL_FAIELD).
                    foreignField(FORIGN_FAIELD).
                    as(AS);

            Aggregation aggregation = Aggregation
                    .newAggregation(Aggregation.match(Criteria.where(PROGRAM_FOR_SUBSCRIPER).exists(true)), lookupOperation);

            List<BasicDBObject> results = mongoOperations.aggregate(aggregation, USER, BasicDBObject.class).getMappedResults();
            log.info("get list of users  ====> ", results);
            generalResponse.setData(results);
        }
        return generalResponse;

    }

    @Override
    public void createUser(CreateUserDto createUserDto) {
        User userSave = new User();
        BeanUtils.copyProperties(createUserDto , userSave);
        userSave.setUserType(UserTypeEnum.TRAINER.getCode());
        userSave.setId(generateUniqueId());
        userSave.setPassword(passwordEncoder.encode(createUserDto.getPassword()));
        userRepo.save(userSave);
    }

    @Override
    public void addProgramm(AddProgrammDto addProgrammDto) {
        log.debug("find trainner id  ====>", addProgrammDto);
        User trainner = userRepo.findUserByIdAndUserType(addProgrammDto.getTrainnerId(), UserTypeEnum.TRAINER.getCode());
        if (Objects.nonNull(trainner)) {
            Program program = new Program();
            log.info(" create Programm and saveing ");
            BeanUtils.copyProperties(addProgrammDto, program);
            program.setId(generateUniqueId());

            log.info(" assign programm to trainner");
            trainner.setPrograms(Collections.singletonList(program));

            programmRepo.save(program);
            userRepo.save(trainner);

        }

    }
    private User getTrainnerById (Long trainnerId) {
        return userRepo.findUserByIdAndUserType(trainnerId , UserTypeEnum.TRAINER.getCode());
    }
    private Long generateUniqueId () {
        return util.getUniqueGenratorId();
    }

}
