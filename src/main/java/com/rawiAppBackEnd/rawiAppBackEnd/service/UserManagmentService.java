package com.rawiAppBackEnd.rawiAppBackEnd.service;

import com.rawiAppBackEnd.rawiAppBackEnd.dto.CreateUserDto;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.SubscriptionProgramDto;
import com.rawiAppBackEnd.rawiAppBackEnd.model.Response.GeneralResponse;

public interface UserManagmentService {

     void addUser (CreateUserDto createUserDto);
     GeneralResponse subscriptionProgram (SubscriptionProgramDto subscriptionProgramDto);

}
