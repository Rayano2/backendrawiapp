package com.rawiAppBackEnd.rawiAppBackEnd.config.hasingPasswordConfig;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.DelegatingPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@AllArgsConstructor
public class HashingPasswordConfig  extends WebSecurityConfigurerAdapter {




    @Bean
    public PasswordEncoder getPasswordEncoder () {
        DelegatingPasswordEncoder encoder = (DelegatingPasswordEncoder) PasswordEncoderFactories.createDelegatingPasswordEncoder();
       encoder.setDefaultPasswordEncoderForMatches(new BCryptPasswordEncoder());
        return encoder;

    }
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().ignoringAntMatchers().disable();

    }


}
