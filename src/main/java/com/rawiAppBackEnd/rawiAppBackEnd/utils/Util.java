package com.rawiAppBackEnd.rawiAppBackEnd.utils;

import com.fasterxml.uuid.Generators;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;
import com.fasterxml.uuid.Generators;

@Component
public class Util {

    @Autowired
    SequenceGenerator sequenceGenerator;



    public Long getUniqueGenratorId () {
         return sequenceGenerator.nextId();
    }

    public static String splitHasingIdPassword (String saltedPassword) { return saltedPassword.substring(8); }
}
