package com.rawiAppBackEnd.rawiAppBackEnd.model;

import com.rawiAppBackEnd.rawiAppBackEnd.entity.Program;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.math.BigDecimal;
import java.util.Date;


@Getter
@Setter
public class Subscription {

    @Id
    private Long subscriptionId;
    private Date subscriptionStart;
    private Boolean active;
    private BigDecimal price;
    @DBRef
    private Program programId;

    public Subscription () {
        if (this.subscriptionStart ==null){
            this.subscriptionStart = new Date();
        }
    }
}
