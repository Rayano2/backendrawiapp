package com.rawiAppBackEnd.rawiAppBackEnd.model.Response;

import java.util.List;

public  class GeneralResponse<T> {

    private T data;
    private List <ErrorResponseModel> errors;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<ErrorResponseModel> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorResponseModel> errors) {
        this.errors = errors;
    }
}
