package com.rawiAppBackEnd.rawiAppBackEnd.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Data
public class AddProgrammDto {

    @NotNull
    private String programmName;
    @NotNull
    @Size(max = 500)
    private String description;
    @NotNull
    private Integer level;
    @NotNull
    private BigDecimal price;
    @NotNull
    private String videoUrl;

    @NotNull
    private Long trainnerId;

}
