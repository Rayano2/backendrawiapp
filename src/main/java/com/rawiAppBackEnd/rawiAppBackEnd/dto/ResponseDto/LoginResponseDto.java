package com.rawiAppBackEnd.rawiAppBackEnd.dto.ResponseDto;

import lombok.Data;

@Data
public class LoginResponseDto {

    private String username;
    private String Status;

}
