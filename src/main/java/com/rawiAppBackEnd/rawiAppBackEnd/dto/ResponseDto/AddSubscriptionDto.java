package com.rawiAppBackEnd.rawiAppBackEnd.dto.ResponseDto;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import java.util.Date;

@Data
public class AddSubscriptionDto {

      private String status;
      @CreatedDate
      private Date subscriptionDateStart;

}
