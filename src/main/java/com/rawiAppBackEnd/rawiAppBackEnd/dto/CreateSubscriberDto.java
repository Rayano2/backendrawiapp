package com.rawiAppBackEnd.rawiAppBackEnd.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CreateSubscriberDto extends CreateUserDto {

    @NotNull
    private Integer age;
    @NotNull
    private Double wieght;
}
