package com.rawiAppBackEnd.rawiAppBackEnd.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

@Getter
@Setter
public class SubscriptionProgramDto {

    @Email
    private String email;

    @Pattern(regexp = "\\d{10}")
    @NotNull
    private String mobileNumber;

    @NotNull
    private String programName;

    @NotNull
    private BigDecimal price;

}
