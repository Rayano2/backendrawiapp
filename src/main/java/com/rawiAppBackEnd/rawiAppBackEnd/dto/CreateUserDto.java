package com.rawiAppBackEnd.rawiAppBackEnd.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Getter
@Setter
public class CreateUserDto {

    @NotNull
    private String name;
    @Email
    @NotNull
    private String email;
    @NotNull
    private String password;
    @Pattern(regexp = "\\d{10}")
    @NotNull
    private String mobileNumber;



}
