package com.rawiAppBackEnd.rawiAppBackEnd.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class GetSubscriberDto {

    @NotNull(message = "id is required")
    private Long trainnerId;


}
