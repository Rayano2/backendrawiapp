package com.rawiAppBackEnd.rawiAppBackEnd.controller;

import com.rawiAppBackEnd.rawiAppBackEnd.dto.CreateUserDto;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.SubscriptionProgramDto;
import com.rawiAppBackEnd.rawiAppBackEnd.service.UserManagmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/v0/userManagment")
@CrossOrigin(origins = "http://localhost:4200")
public class UserManagmentController {

    private UserManagmentService userManagmentService;

    @Autowired
    public UserManagmentController (UserManagmentService userManagmentService) {
        this.userManagmentService = userManagmentService;
    }


    @PostMapping(value = "/createSubscriber"  , consumes = "application/json" , produces = "application/json")
    public ResponseEntity  addUser (@Valid  @RequestBody CreateUserDto createUser) {
        userManagmentService.addUser(createUser);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{UserName}")
                .buildAndExpand(createUser.getEmail())
                .toUri();
        return  ResponseEntity.created(location).build();
    }

    @PostMapping(value = "/subscriptionProgram" , consumes = "application/json" , produces = "application/json")
    public ResponseEntity subscriptionProgram (@Valid @RequestBody SubscriptionProgramDto subscriptionProgramDto ) {
        return new ResponseEntity(userManagmentService.subscriptionProgram(subscriptionProgramDto) , HttpStatus.OK);
    }


}
