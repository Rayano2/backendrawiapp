package com.rawiAppBackEnd.rawiAppBackEnd.controller;


import com.rawiAppBackEnd.rawiAppBackEnd.dto.AddProgrammDto;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.CreateUserDto;
import com.rawiAppBackEnd.rawiAppBackEnd.dto.GetSubscriberDto;
import com.rawiAppBackEnd.rawiAppBackEnd.service.AdminManagmentService;
import com.rawiAppBackEnd.rawiAppBackEnd.utils.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/api/v0/adminManagment")
@CrossOrigin(origins = "http://localhost:4200")
@Slf4j
public class AdminManagmentController {

    private AdminManagmentService adminManagmentService;
    private Util util;

    @Autowired
    public AdminManagmentController (AdminManagmentService adminManagmentService , Util util) {
        this.adminManagmentService = adminManagmentService;
        this.util = util;
    }

    @GetMapping(value = "/getSubscriber" , consumes = "application/json" , produces = "application/json")
    public ResponseEntity getSubscriber (@RequestBody GetSubscriberDto getSubscriberDto) {
           return new ResponseEntity(adminManagmentService.getSubscriber(getSubscriberDto) , HttpStatus.OK);

    }
    @PostMapping(value = "/createAdmin"  , consumes = "application/json" , produces = "application/json")
    public ResponseEntity  createAdmin (@Valid  @RequestBody CreateUserDto createUser) {
        adminManagmentService.createUser(createUser);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{UserName}")
                .buildAndExpand(createUser.getName())
                .toUri();
        return  ResponseEntity.created(location).build();

    }

    @PostMapping(value = "/addProgramm"  , consumes = "application/json" , produces = "application/json")
    public ResponseEntity  addProgramm (  @RequestBody AddProgrammDto addProgrammDto) {
        log.info("inside add Programm controller ====>", addProgrammDto);
        adminManagmentService.addProgramm(addProgrammDto);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{prorgrammName}")
                .buildAndExpand(addProgrammDto.getProgrammName())
                .toUri();
        return  ResponseEntity.created(location).build();

    }


}
