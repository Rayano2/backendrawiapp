package com.rawiAppBackEnd.rawiAppBackEnd.controller;


import com.rawiAppBackEnd.rawiAppBackEnd.dto.LoginDto;
import com.rawiAppBackEnd.rawiAppBackEnd.model.Response.GeneralResponse;
import com.rawiAppBackEnd.rawiAppBackEnd.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v0")
@CrossOrigin(origins = "http://localhost:4200")
public class LoginController {


    LoginService loginService;
    @Autowired
    public LoginController (LoginService loginService) {
        this.loginService = loginService;
    }


    @PostMapping(value = "/login"  , consumes = "application/json" , produces = "application/json")
    public ResponseEntity <GeneralResponse> checkLogin (@RequestBody LoginDto loginDto) {
        return new ResponseEntity (loginService.checkLogin(loginDto) , HttpStatus.OK);
    }
}
