package com.rawiAppBackEnd.rawiAppBackEnd.enums;

public enum EstatusLogin {

    SUCCESS(1 , "User Loged in Success"),
    FAIELD (2  , "User Not Found");

    private Integer statusCode;
    private String message;

    EstatusLogin(Integer statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
