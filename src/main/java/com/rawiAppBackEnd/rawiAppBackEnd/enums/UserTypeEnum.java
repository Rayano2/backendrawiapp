package com.rawiAppBackEnd.rawiAppBackEnd.enums;

public enum  UserTypeEnum {
    TRAINER(1 , "trainer"),
     USER(2 , "user");

    private Integer code;
    private String userType;

    UserTypeEnum(Integer code, String userType) {
        this.code = code;
        this.userType = userType;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
